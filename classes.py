class Person(object):
    def __init__(self):
        self.get_name()
        self.get_age()
        self.show_age_and_name()

    def get_age(self):
        self.age = input('What is your age? ')
    
    def get_name(self):
        self.name = input('What is your name? ')

    def show_age_and_name(self):
        print('{} is {}'.format(
            self.name, 
            self.age
            )
        )

if __name__ == "__main__":
    Person()
