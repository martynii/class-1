--Lesson 1
```
#Download python 3 https://www.python.org/downloads/

#Install pip3
wget https://bootstrap.pypa.io/get-pip.py ; python3 get-pip.py


#Install virtualenvwrapper
pip3 install virtualenvwrapper

cat /usr/local/bin/virtualenvwrapper.sh >> ~/.bash_profile
bash #or restart terminal

#Test installation
mkvirtualenv test

python --version #should be python3

pip install requests #should not need to be done as root/sudo

pip freeze #should only display your installed version of requests 
```
