def get_name():
    return input('What is your name? ')

def get_age():
    return input('What is your age? ')


def show_age_and_name(name, age):
    return '{} is {}'.format(name, age)

def main():
    print(show_age_and_name(
        get_name(),
        get_age()
        )
    )

if __name__ == "__main__":
    main()
